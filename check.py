#!/usr/bin/env python3

import requests
import sys
import re

class Args():
    def usage():
        print('Usage:\n  ./check.py <regex> <bandcamp_url>',
              '[<regex>, <bandcamp_url>, ...]')

    def check_args():
        if len(sys.argv) < 3 \
            or (len(sys.argv) % 2 == 0 and '--inv' not in sys.argv):
            Args.usage()
            exit(1)

class Checker:
    def __init__(self, args):
        self.inverse = False
        if args[0] == '--inv':
            self.inverse = True
            args = args[1:]

        self.regexes = args[::2]
        self.urls = args[1::2]

    def search(self):
        self.session = requests.Session()
        headers = {'User-Agent': 'Mozilla/5.0 (Macintosh;' + 
                                 ' Intel Mac OS X 10_10_1) AppleWebKit/537.36'+
                                 ' (KHTML, like Gecko) Chrome/39.0.2171.95' + 
                                 ' Safari/537.36'}

        for url, regex in zip(self.urls, self.regexes):
            r = self.session.get(url, headers=headers)

            reg = re.compile(regex)
            results = reg.findall(r.text, re.IGNORECASE)

            if bool(results) ^ self.inverse:
                res = '({})'.format(', '.join([str(r) for r in results]))
                # Do something interesting, like send a SMS
                print('Found "{}" in "{}"'.format(res, url))


if __name__ == "__main__":
    Args.check_args()

    bandcamp = Checker(sys.argv[1:])
    bandcamp.search()
